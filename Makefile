﻿all: crawl

merger-serials:
	@python -m vxa.analyser.merger

normalize-person-names:
	@python -m vxa.analyser.person_normalizer

normalize-category-names:
	@python -m vxa.analyser.category_normalizer

crawl: tvzavr.ru now.ru ivi.ru tvigle.ru megogo.net

tvzavr.ru:
	@scrapy crawl tvzavr.ru

now.ru:
	@scrapy crawl now.ru

ivi.ru:
	@scrapy crawl ivi.ru

tvigle.ru:
	@scrapy crawl tvigle.ru

megogo.net:
	@scrapy crawl megogo.net

init:
	@python -m vxa.models.__init__

clean_all: clean

clean:
	@find ./ -name "*~" -type f -exec rm -f {} \;
	@find ./ -name "*.pyc" -type f -exec rm -f {} \;
	@find ./ -name "*.pyo" -type f -exec rm -f {} \;
	@find ./ -name __pycache__ -type d -exec rm -rf {} \;

install:
	@pip install -r requirements.txt

.PHONY: all install clean clean_all
