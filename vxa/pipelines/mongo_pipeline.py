# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from __future__ import absolute_import, division, print_function

import logging
import pymongo
import copy

from scrapy.exceptions import DropItem
from scrapy.exporters import PythonItemExporter

from vxa.models import VideoRaw

from vxa.settings.storage import MONGO


class MongoPipeline(object):
    """
        Saves videos to Mongo
    """

    __logger = logging.getLogger(__name__)

    def __init__(self):
        self.exporter = PythonItemExporter()
        connection = pymongo.MongoClient(
            MONGO['HOST'],
            MONGO['PORT']
        )
        db = connection[MONGO['DATABASE']]
        self.collection = db[MONGO['COLLECTION']]

    def process_item(self, aitem, spider):
        item = copy.deepcopy(aitem)
        item = self.exporter.export_item(item)
        for data in item:
            if not data:
                raise DropItem("Missing data!")
        hash_md5 = VideoRaw.get_hash_by_args(**item)
        item['hash_md5'] = hash_md5
        self.collection.update(
            {'hash_md5': hash_md5},
            item,
            upsert=True
        )
        self.__logger.debug(
            "video %s added to MongoDB database!"%(hash_md5),
        )
        return aitem
