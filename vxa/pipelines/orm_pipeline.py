# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


from __future__ import absolute_import, division, print_function

import logging
import string
import copy
import random

from scrapy.exporters import PythonItemExporter

from vxa.models import TagType
from vxa.models import Tag
from vxa.models import VideoRaw
from vxa.models import VideoRawToTag
from vxa.models import Orm




from vxa.settings.logging import start_logging



def _get_random_string(n=64):
    return "NONE: %s" % (''.join(random.choice(string.printable) for _ in range(n)))


class OrmPipeline(object):
    """
        Saves videos to sql data base within Orm.
    """

    __logger = logging.getLogger(__name__)

    def __init__(self):
        self.orm = Orm()
        self.exporter = PythonItemExporter()

    def process_item(self, aitem, spider):
        item = copy.deepcopy(aitem)
        item = self.exporter.export_item(item)
        session = self.orm.get_session()
        tag_desc_list = item.pop('tag_list', None)
        item.pop('unknown_field_dict', None)
        video_raw = VideoRaw.as_unique(
            session,
            **item
        )
        tag_list = []
        video2tag_list = []
        for tag_desc in tag_desc_list:
            name = tag_desc.pop('name', None)
            vendor_url = tag_desc.pop('vendor_url', None)
            if not name:
                name = _get_random_string()
            tag_type_name = tag_desc.pop('tag_type_name', 'tag')
            tag_type = TagType.cached_name(session, tag_type_name)

            tag = Tag(
                name=name,
                vendor_url=vendor_url,
                tag_type_id=tag_type.id,
            )
            video2tag = VideoRawToTag(
                video_raw_hash_md5=video_raw.hash_md5,
                tag_hash_md5=tag.hash_md5,
            )
            tag_list += [tag]
            video2tag_list += [video2tag]
        Tag.bulk_insert(session, tag_list)
        VideoRawToTag.bulk_insert(session, video2tag_list)
        session.flush()
        session.close()
        self.__logger.debug(
            "video %s added to SQL database!"%(video_raw.hash_md5),
        )
        return aitem
