#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from time import gmtime, strftime
load_time = strftime("%Y.%m.%d %H:%M:%S", gmtime())


#sys.stdout = Unbuffered(sys.stdout)

#from gevent import monkey
#monkey.patch_all()

import time
import re

import requests
import requests.exceptions

from ..models import VideoRaw
from ..models import Orm

from .uploader import Uploader

orm = Orm()


def test():
    session = orm.get_scoped_session()
    session = session()

    uploader = Uploader('http://webi.2in2.ru/api.php', 'W-495', 'KvErTi12')

    # video_id_list =  [
    #     video_id[0]
    #     for video_id in session.query(
    #         VideoToVideo.video_id
    #     ).filter(VideoToVideo.version != 6 ).all()
    # ]

    video_list =  session.query(VideoRaw)                   \
        .filter(VideoRaw.vendor_url == 'tvzavr.ru')         \
        .filter(VideoRaw.upload_date > 20150500)            \
        .filter(VideoRaw.content_id < 20868)                \
        .order_by(VideoRaw.content_id.desc())               \
        .all()

    for video in video_list:
        try_handle_video((video, uploader),)


def try_handle_video((video, uploader),):
    try:
        handle_video((video, uploader),)
        #time.sleep(10)
    except requests.exceptions.ConnectionError:
        time.sleep(20)

def xhandle_video((video, uploader),):
    page_url_list = video.page_url.split('/')
    tvzavr_url  = page_url_list[-1:][0]
    ivi_url     = ivi_replace(video.copy_list[0].title)
    self_url    = vxa_build_url(video.title)

    print ivi_url, self_url

    #uploader.write(tvzavr_url, '#перенаправление [[%s]]'%(self_url))
    res = uploader.write(ivi_url, '#перенаправление [[%s]]'%(self_url))
    print res

    print ivi_url, self_url, 'ok'

def handle_video((video, uploader),):



    session = orm.get_scoped_session()
    session = session()

    page_url_list = video.page_url.split('/')
    tvzavr_url  = page_url_list[-1:][0]
    if(video.copy_list):
        ivi_url     = ivi_replace(video.copy_list[0].title)
    self_url    = vxa_build_url(video.title)


    outstring = '''
{{!:advideo-vxa-before-toc-1}}
{{!:advideo-vxa-before-toc-2}}
__TOC__\n'''


    orig_string = ''
    if(video.original_title):
        outstring += '[[Категория:%s]]'%(video.original_title)
        orig_string = " (%s)"%(video.original_title)

    outstring += '''


{{!:advideo-vxa-before-main-title-1}}
{{!:advideo-vxa-before-main-title-2}}

== Фильм «%s%s» ==
{{!:advideo-vxa-after-main-title-1}}
{{!:advideo-vxa-after-main-title-2}}
'''%(video.title, orig_string)

    vi_description_list = []
    for vi in video.copy_list:
        vi_description = vi.description
        if(vi.vendor_url == 'ivi.ru'):
            vi_description = ivi_crop(vi_description)
        if( vi_description not in vi_description_list):
            vi_description_list += [vi_description]
            outstring += "\n%s\n"%(ivi_crop(vi.description))

    outstring += '''
=== Кино «{url}» на tvzavr.ru  ===
{{{{!:advideo-vxa-before-tvzavr-iframe-1}}}}

{release_year}{upload_year}Средняя оценка картины: {rating} из {rating_count}. Просмотры: {views_total}.
<div class="tvzavr-iframe">
<iframe width="100%%" height="522px" >http://www.tvzavr.ru/action/window/{content_id}?loc=http://webi.2in2.ru/{url}</iframe>
{{{{!:videocopyright|url={page_url}|source={source_url}|vender=tvzavr}}}}
</div>
{{{{!:advideo-vxa-after-tvzavr-iframe-1}}}}
'''.format(
        id          = video.id,
        url         = video.title,
        content_id  = video.content_id,
        page_url    = video.page_url,
        source_url  = video.page_url,
        duration_hour  = video.duration / 3600 ,
        duration_min   = (video.duration % 3600) / 60,
        duration_sec   = video.duration % 60,
        rating          = video.rating,
        rating_count    = video.rating_count,
        views_total     = video.views_total,
        release_year = '%s. '%(video.release_date.year) if video.release_date else '',
        upload_year = '%s. '%(video.upload_date.year) if video.upload_date else '',
    )

    outstring += '''
{{!:advideo-vxa-before-main-description-1}}
{{!:advideo-vxa-before-main-description-2}}

%s

{{!:advideo-vxa-after-main-description-1}}
'''%(tvzavr_crop(video.description))

    xtag_list = list(video.tag_list)

    for i, vi in enumerate(video.copy_list):
        description = vi.description

        if('ivi.ru' == vi.vendor_url):
            description = ivi_crop(description)

            outstring += '''
=== Видео «{title}» на ivi.ru ===
{{{{!:advideo-vxa-after-ivi-title-1}}}}
{{{{!:advideo-vxa-after-ivi-title-2}}}}

{description}

{{{{!:advideo-vxa-after-ivi-description-2}}}}

{{{{!:advideo-vxa-before-ivi-iframe-{i}-1}}}}
<div class="ivi-iframe">
<iframe width="100%%" height="522px" >http://www.ivi.ru/video/player?videoId={content_id}&sharing_to=webi</iframe>
{{{{!:videocopyright|url={page_url}|source={source_url}|vender=ivi.ru}}}}
</div>
{{{{!:advideo-vxa-after-ivi-iframe-{i}-1}}}}
'''.format(
                i           = i,
                description = description,
                id          = vi.id,
                title         = vi.title,
                content_id  = vi.content_id,
                page_url    = vi.page_url,
                source_url  = vi.source_url,
            )

        if('+++megogo.net' == vi.vendor_url and vi.player_url):

            player_list = re.split('\W+', vi.player_url)[10:]

            if(player_list):
                player_id = player_list[1]

                outstring += '''
=== Фильм «{title}» на megogo.net ===
{{{{!:advideo-vxa-after-megogo-title-1}}}}
{{{{!:advideo-vxa-after-megogo-title-2}}}}

{description}

{{{{!:advideo-vxa-after-megogo-description-1}}}}

{{{{!:advideo-vxa-before-megogo-iframe-1}}}}
<div class="megogo-iframe">
<iframe width="100%%" height="522px" >http://megogo.net/e/{player_id}&sharing_to=webi</iframe>
{{{{!:videocopyright|url={page_url}|source={source_url}|vender=megogo.net}}}}
</div>
{{{{!:advideo-vxa-after-megogo-iframe-1}}}}
'''.format(
                    i           = i,
                    description = description,
                    id          = vi.id,
                    title         = vi.title,
                    player_id   = player_id,
                    page_url    = vi.page_url,
                    source_url  = vi.source_url,
                )


        if('now.ru' == vi.vendor_url and vi.player_url):

            outstring += '''
=== Фильм «{title}» на now.ru ===
{{{{!:advideo-vxa-after-now-title-1}}}}
{{{{!:advideo-vxa-after-now-title-2}}}}

{description}

{{{{!:advideo-vxa-after-now-description-1}}}}

{{{{!:advideo-vxa-before-now-iframe-1}}}}
<div class="now-iframe">
<iframe width="100%%" height="522px" >{player_url}</iframe>
{{{{!:videocopyright|url={page_url}|source={source_url}|vender=now.ru}}}}
</div>
{{{{!:advideo-vxa-after-now-iframe-1}}}}
'''.format(
                i           = i,
                description = description,
                id          = vi.id,
                title         = vi.title,
                player_url  = vi.player_url,
                page_url    = vi.page_url,
                source_url  = vi.source_url,
            )

    subseries_list =  session.query(VideoRaw)          \
        .filter(VideoRaw.vendor_url == 'tvzavr.ru')      \
        .filter(VideoRaw.parent_title == video.title)      \
        .order_by(VideoRaw.episode)   \
        .all()

    parent_title =  None
    if(subseries_list):
        parent_title = video.title

    if (video.parent_title):
        parent_title = video.parent_title

    if (parent_title):
        series_list = []
        if(subseries_list):
            series_list = subseries_list
        else:
            series_list =  session.query(VideoRaw)          \
                .filter(VideoRaw.vendor_url == 'tvzavr.ru')      \
                .filter(VideoRaw.parent_title == parent_title)      \
                .order_by(VideoRaw.episode)   \
                .all()

        if(series_list):
            outstring += '''
{{!:advideo-vxa-before-also-title-1}}
{{!:advideo-vxa-before-also-title-2}}
=== Другие серии картины «%s» ===
{{!:advideo-vxa-after-also-title-1}}
{{!:advideo-vxa-after-also-title-2}}
[[Категория:%s]]

'''%(video.parent_title, video.parent_title)
            outstring += ''.join([
                '* [[%s|%s]];\n'%(vxa_build_url_title(series.title), series.title)
                for series in series_list
            ])
            if(video.parent_title):
                outstring += '* [[%s|%s]].\n'%(vxa_build_url_title(parent_title), video.parent_title)
            outstring += '''

{{!:advideo-vxa-after-also-list-1}}

'''

    for vi in video.copy_list:
        xtag_list += list(vi.tag_list)

    res_dict = handle_tag_list(unique(xtag_list), video.title)
    outstring +=  res_dict.string_dict.sum_string

    thumbnail_list = video.thumbnail_url.split('/')
    thumbnail_width, thumbnail_height =   tuple(thumbnail_list[-2].split('x'))

    outstring += '''
{{{{!:advideo-vxa-at-the-end-1}}}}
{{{{!:advideo-vxa-at-the-end-2}}}}

<keywords  name="title" content="{title} смотреть кино онлайн бесплатно в отличном качестве на webi.2in2.ru!"/>
<keywords  name="description" content="{description}"/>
<keywords  name="x-keywords" content="видео, {title2} онлайн, {parent_title} смотреть, {tag_string}, {original_title} смотреть онлайн, {title} бесплатно"/>
<keywords  name="og:site_name" content="webi.2in2.ru"/>
<keywords  name="og:url" content="http://webi.2in2.ru/{url}?id={id}&x={{{{RANDOM}}}}"/>
<keywords  name="og:title" content="«{original_title}» смотреть видео онлайн бесплатно в хорошем качестве на webi.2in2.ru!"/>
<keywords  name="og:type" content="video.movie"/>
<keywords  name="og:duration" content="{og_duration}"/>
<keywords  name="og:image" content="{og_thumbnail}"/>
<keywords  name="og:image:secure_url" content="{og_thumbnail_https}"/>
<keywords  name="og:image:type" content="image/jpeg"/>
<keywords  name="og:image:width" content="{og_thumbnail_width}"/>
<keywords  name="og:image:height" content="{og_thumbnail_height}"/>
<keywords  name="og:description" content="{og_description}"/>
<keywords  name="og:video" content="http://www.tvzavr.ru/js/v5/tvzavrplayer2.swf?playlist={content_id}&amp;autoplay=1&amp;externalinterface=0&amp;f=0"/>
<keywords  name="og:video:secure_url" content="https://www.tvzavr.ru/js/v5/tvzavrplayer2.swf?playlist={content_id}&amp;autoplay=1&amp;externalinterface=0&amp;f=0"/>
<keywords  name="og:video:type" content="application/x-shockwave-flash"/>
<keywords  name="og:video:width" content="1920"/>
<keywords  name="og:video:height" content="1080"/>
<keywords  name="og:video:duration" content="{og_duration}"/>
<keywords  name="video:duration" content="{video_duration}"/>
<keywords  name="fb:app_id" content="174350282734989"/>
<keywords  name="twitter:card" content="summary"/>
<keywords  name="twitter:site" content="@tvzavr"/>
<keywords  name="twitter:title" content="{title}"/>
<keywords  name="twitter:description" content="{twitter_description}"/>
<keywords  name="twitter:creator" content="@tvzavr"/>
<keywords  name="twitter:image:src" content="{tvitter_thumbnail}"/>
<keywords  name="twitter:domain" content="http://webi.2in2.ru/">
<link  rel="image_src" href="{vk_thumbnail}"/>
<link  rel="video_src" href="http://www.tvzavr.ru/js/v5/tvzavrplayer2.swf?playlist={content_id}&amp;autoplay=1&amp;externalinterface=0&amp;tag=video_src&amp;f=0"/>
</keywords>
<keywords  name="video_type" content="application/x-shockwave-flash"/>
<keywords  name="video_height" content="1920"/>
<keywords  name="video_width" content="1080"/>
<keywords  name="mrc__share_title" content="{mrc_title} на webi.2in2.ru!"/>
<keywords  name="mrc__share_description" content="{mrc_description}"/>

[[Категория:{title}]]
{rcat_year}
{ucat_year}

Время {load_time} / {ctime}
'''.format(
    rcat_year = '[[Категория:%s]]'%(video.release_date.year) if video.release_date else '',
    ucat_year = '[[Категория:%s]]'%(video.upload_date.year) if video.upload_date else '',

    title = video.title,
    description = seo_crop_1(video.copy_list[0].description if video.copy_list else video.description, 250),
    title2 = video.copy_list[0].title if video.copy_list else video.title,
    parent_title = video.parent_title if video.parent_title else video.title,
    original_title = video.original_title if video.original_title else video.title,
    tag_string = res_dict.string_dict.tagseo_string,
    og_description = seo_crop_1(video.description),

    mrc_title = "%s (%s)"%(video.original_title, video.title) if video.original_title else video.title,
    mrc_description = seo_crop_1(video.description, 500),
    load_time = load_time,
    ctime = strftime("%Y.%m.%d %H:%M:%S", gmtime()),
    twitter_description = seo_crop_1(video.copy_list[0].description, 1000) if video.copy_list else seo_crop_1(video.description, 200),
    url = self_url,
    id = video.id,
    content_id = video.content_id,
    og_duration = video.duration,
    video_duration = video.copy_list[0].duration if video.copy_list else video.duration,
    og_thumbnail_width = thumbnail_width,
    og_thumbnail_height = thumbnail_height,
    og_thumbnail = video.thumbnail_url.replace('https', 'http'),
    og_thumbnail_https = video.thumbnail_url.replace('http', 'https'),
    vk_thumbnail = video.thumbnail_url.replace('http', 'https'),
    tvitter_thumbnail = video.copy_list[0].thumbnail_url.replace('http', 'https') if video.copy_list else video.thumbnail_url.replace('http', 'https'),
)

    print video.id, video.content_id, self_url, "wait ..."

    uploader.write(tvzavr_url, '#перенаправление [[%s]]'%(self_url))
    if(video.copy_list):
        uploader.write(ivi_url, '#перенаправление [[%s]]'%(self_url))

    res = uploader.write(self_url, outstring)

    for genre in res_dict.list_dict.genre_list:
        uploader.write('Категория:%s'%(genre), '{{!:advideo-vxa-cat-genre-1}}\n[[Категория:Жанры]]')

    for category in res_dict.list_dict.category_list:
        uploader.write('Категория:%s'%(category), '{{!:advideo-vxa-cat-category-1}}\n[[Категория:Категории]]')

    if (video.title):
        uploader.write('Категория:%s'%(video.title), '{{!:advideo-vxa-cat-parent-title-1}}\n[[Категория:Фильмы]]')

    if (video.parent_title):
        uploader.write('Категория:%s'%(video.parent_title), '{{!:advideo-vxa-cat-parent-title-1}}\n[[Категория:Сериалы]]')

    if (video.original_title):
        uploader.write('Категория:%s'%(video.original_title), '{{!:advideo-vxa-cat-original-title-1}}\n[[Категория:Original_title]]')

    if (video.release_date):
        uploader.write('Категория:%s'%(video.release_date.year), '{{!:advideo-vxa-cat-year-1}}\n[[Категория:Год]]')

    if (video.upload_date):
        uploader.write('Категория:%s'%(video.upload_date.year), '{{!:advideo-vxa-cat-year-1}}\n[[Категория:Год]]')

    for script in res_dict.list_dict.script_list:
        uploader.write('Категория:%s'%(script), '{{!:advideo-vxa-cat-script-1}}\n[[Категория:Сценаристы]]')

    for actor in res_dict.list_dict.actor_list:
        uploader.write('Категория:%s'%(actor), '{{!:advideo-vxa-cat-actor-1}}\n[[Категория:Актёры]]')

    for director in res_dict.list_dict.director_list:
        uploader.write('Категория:%s'%(director), '{{!:advideo-vxa-cat-director-1}}\n[[Категория:Режиссёры]]')

    print video.id, video.content_id, self_url, "ok"

def ivi_replace(title):
    xtitle = title.replace(' ', '_').replace('.', '_⊙').replace('?', '⊙')
    return xtitle

def tvzavr_crop(description):
    description = description.replace('TVzavr.ru!', 'webi.2in2.ru.')
    return description

def ivi_crop(description):
    description = vxa_replace(description)
    description = "%s."%(re.sub('^(.+?[.,:]+)', '', description[::-1], re.U)[::-1])
    return description

def seo_crop_1(description, size = 250):
    description = description.replace("'", '').replace('"', '')
    description = vxa_replace(description[0:size])
    description = "%s."%(re.sub('^(.+?[.,:]+)', '', description[::-1], re.U)[::-1])
    return description

def seo_crop_space(description, size = 250):
    description = vxa_replace(description[0:size])
    description = "%s."%(re.sub('^(.+?[.,:\ ]+)', '', description[::-1], re.U)[::-1])
    return description

def vxa_replace(description):
    description = description.replace('"','')
    return description


def vxa_build_url_title(title):
    title = title.strip()
    title = title.replace('.',':').replace('?',':')
    if ':' == title[-1]:
        title = title[:-1]
    return title

def vxa_build_url(title):
    title = vxa_build_url_title(title).replace(' ', '_')
    return title


class SmartDict(dict):
    def __init__(self, *args, **kwargs):
        self.__dict__ = kwargs
        super(SmartDict, self).__init__(*args, **kwargs)


def handle_tag_list(tag_list, title = ''):

    outstring = ""

    list_dict = SmartDict(
        script_list = [],
        actor_list = [],
        director_list = [],
        genre_list = [],
        category_list = [],
        oth_list = [],
        tagcat_list = [],
    )


    for tag in tag_list:
        if(tag.tag_type_id in [2, 5, 6, 7, 17, 19, 28]):
            continue

        tag_type_id = tag.tag_type_id
        if(tag.norm_tag):
            tag = tag.norm_tag

        tag_name = filter_chars(tag.name)

        if (tag_type_id == 4 ):
            list_dict.category_list += [lower(tag_name)]

        elif (tag_type_id == 10):
            list_dict.actor_list += [tag_name]

        elif (tag_type_id == 11):
            list_dict.script_list += [tag_name]

        elif (tag_type_id == 12):
            list_dict.actor_list += [tag_name]

        elif (tag_type_id == 13):
            list_dict.director_list += [tag_name]

        elif (tag_type_id == 14 ):
            list_dict.category_list += [lower(tag_name)]

        elif (tag_type_id == 15 ):
            list_dict.category_list += [lower(tag_name)]

        elif (tag_type_id == 16 ):
            list_dict.genre_list += [lower(tag_name)]

        else:
            list_dict.category_list += [lower(tag_name)]

    list_dict.tagcat_list = list_dict.category_list     \
                            + list_dict.actor_list      \
                            + list_dict.script_list     \
                            + list_dict.director_list   \
                            + list_dict.genre_list

    string_dict = SmartDict(
        director_string = '',
        actor_string = '',
        script_string = '',
        genre_string = '',
        category_string = '',
        tagcat_string = '',
        sum_string = '',
        tagseo_string = ''
    )


    if(list_dict.director_list):
        string_dict.director_string += '\n== Режиссёры видеофильма «%s» ==\n'%(title)
        string_dict.director_string += ';\n'.join([
            "* [[:Категория:%s|%s]]"%(director, director)
            for director in sorted(unique(list_dict.director_list))
        ])
        string_dict.director_string += '.\n'
        string_dict.sum_string += string_dict.director_string

    if(list_dict.actor_list):
        string_dict.actor_string = '\n== Актёры ленты «%s» ==\n{{!:advideo-vxa-before-actor-list-1}}\n'%(title)
        string_dict.actor_string += ';\n'.join([
            "* [[:Категория:%s|%s]]"%(actor, actor)
            for actor in sorted(unique(list_dict.actor_list))
        ])
        string_dict.actor_string += '.\n{{!:advideo-vxa-after-actor-list-1}}\n'
        string_dict.sum_string += string_dict.actor_string

    if(list_dict.script_list):
        string_dict.script_string = '\n== Cценаристы кинофильма«%s» ==\n{{!:advideo-vxa-before-script-list-1}}\n'%(title)
        string_dict.script_string += ';\n'.join([
            "* [[:Категория:%s|%s]]"%(script, script)
            for script in sorted(unique(list_dict.script_list))
        ])
        string_dict.script_string += '.\n{{!:advideo-vxa-after-script-list-1}}\n'
        string_dict.sum_string += string_dict.script_string

    if(list_dict.genre_list):
        string_dict.genre_string = '\n== Жанры видео «%s» ==\n{{!:advideo-vxa-before-genre-list-1}}\n'%(title)
        string_dict.genre_string += ';\n'.join([
            "* [[:Категория:%s|%s]]"%(genre, genre)
            for genre in sorted(unique(list_dict.genre_list))
        ])
        string_dict.genre_string += '.\n{{!:advideo-vxa-after-genre-list-1}}\n'
        string_dict.sum_string += string_dict.genre_string


    if(list_dict.category_list):
        string_dict.category_string = '\n== Категории фильма «%s» ==\n{{!:advideo-vxa-before-category-list-1}}\n'%(title)
        string_dict.category_string += ';\n'.join([
            "* [[:Категория:%s|%s]]"%(category, category)
            for category in sorted(unique(list_dict.category_list))
        ])
        string_dict.category_string += '.\n{{!:advideo-vxa-after-category-list-1}}\n\n'
        string_dict.sum_string += string_dict.category_string

    if(list_dict.tagcat_list):
        string_dict.tagcat_string += '\n'.join([
            "[[Категория:%s]]"%(tagcat)
            for tagcat in sorted(unique(list_dict.tagcat_list))
        ])
        string_dict.sum_string += string_dict.tagcat_string



    tagseo_list = list_dict.category_list + list_dict.genre_list
    if(list_dict.category_list):
        string_dict.tagseo_string += ', '.join([
            tagseo for tagseo in sorted(unique(tagseo_list))
        ])

    res_dict = SmartDict(
        list_dict = list_dict,
        string_dict = string_dict,
    )

    return res_dict



def filter_chars(text):
    return text.replace('+', '⨁')


def lower(string):
    return unicode(string, 'utf8').lower().encode('utf8')


def unique(a):
    seen = set()
    return [seen.add(x) or x for x in a if x not in seen]


test()
