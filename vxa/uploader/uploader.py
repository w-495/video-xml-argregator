#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import

import requests
import json
import copy


def get_in_dict(dict_, path):
    path_list = path.split('/')
    res = copy.deepcopy(dict_)
    for node in path_list:
        if (not res):
            return None
        if node == 'value()':
            values = res.values()
            if not values:
                return None
            res = values[0]
            continue
        res = res.get(node)
    return res


class Uploader(object):
    """
        Simple API:Client code For Uploading Texts To Mediawiki 1.5

        uploader = Uploader('http://wikipedia', 'User', 'PaS$w0Rd')
        uploader.write('My Article title', 'My Article Body')
    """

    def __init__(self, url, name, password):
        self.url = url
        self.name = name
        self.password = password

        lgtoken = self.get_lgtoken(url, name, password)
        cookies = self.login(url, name, password, lgtoken)

        edittoken = None
        if (cookies):
            edittoken = self.get_edittoken(url, cookies)

        self.lgtoken = lgtoken
        self.cookies = cookies
        self.edittoken = edittoken

    def get_lgtoken(self, url, name, password):
        handshake_response = requests.post(
            "{URL}?action=login&format=json".format(
                URL=url
            ),
            data=dict(
                lgname=name,
                lgpassword=password,
            ),
        )

        handshake_dict = json.loads(handshake_response.content)
        lgtoken = get_in_dict(handshake_dict, 'login/lgtoken')
        return lgtoken

    def login(self, url, name, password, lgtoken):
        lonin_response = requests.post(
            "{URL}?action=login&format=json".format(
                URL=url
            ),
            data=dict(
                lgname=name,
                lgpassword=password,
                lgtoken=lgtoken
            ),
        )
        lonin_dict = json.loads(lonin_response.content)
        if 'Success' == get_in_dict(lonin_dict, 'login/result'):
            return lonin_response.cookies
        return None

    def get_edittoken(self, url, cookies):
        editor_response = requests.post(
            "{URL}?action=query&prop=info&intoken=edit&titles=page&format=json".format(
                URL=url,
            ),
            cookies=cookies
        )
        editor_dict = json.loads(editor_response.content)
        edittoken = get_in_dict(editor_dict, 'query/pages/value()/edittoken')
        return edittoken

    def _write(self, url, title, text, edittoken, cookies):
        page_response = requests.post(
            "{URL}?action=edit&format=json".format(
                URL=url,
            ),
            data=dict(
                token=edittoken,
                title=title,
                text=text
            ),
            cookies=cookies
        )
        status = page_response.content
        try:
            status = json.loads(page_response.content)
        except ValueError:
            pass
        return status

    def write(self, title, text):
        return self._write(self.url, title, text, self.edittoken, self.cookies)
