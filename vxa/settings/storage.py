# -*- coding: utf-8 -*-


from __future__ import absolute_import, division, print_function


MONGO = dict(
    HOST='127.0.0.1',
    PORT=27017,
    DATABASE='vxa',
    COLLECTION='video',
)

SQL = dict(
    HOST='127.0.0.1',
    USER='vxa',
    PASSWD='video_xml_argregator',
    DATABASE='video_xml_argregator',
)
