# -*- coding: utf-8 -*-

# Scrapy settings for vxa project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

from __future__ import absolute_import, division, print_function

BOT_NAME = 'vxa'

SPIDER_MODULES = [
    'vxa.spiders'
]

NEWSPIDER_MODULE = 'vxa.spiders'

ITEM_PIPELINES = {
    'vxa.pipelines.OrmPipeline': 1,
    'vxa.pipelines.MongoPipeline': 2,
}

DOWNLOAD_HANDLERS = {
    's3': None,
}

USER_AGENT = u'VXA \ Video eXchange Agregator [mailto: w@w-495.ru]'


