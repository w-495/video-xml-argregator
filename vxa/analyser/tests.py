#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import absolute_import

import time
import sys
import re
import six
import itertools

import thread
import threading
import multiprocessing.dummy

from sqlalchemy import or_
from sqlalchemy import and_
from sqlalchemy import func

from ..models import TagType
from ..models import Tag
from ..models import VideoRaw
from ..models import VideoRawToTag
from ..models import VideoToVideo
from ..models import Orm

orm = Orm()

#
# def mine_normalized_category():
#


def mine_normalized_video___():
    session = orm.get_session()
    vendor_list = [
        'tvzavr.ru',
        'tvigle.ru',
    ]
    vendor1_vlist = session.query(VideoRaw).filter(VideoRaw.vendor_url == 'ivi.ru').all()
    cnt = 0
    for vendor1_video in vendor1_vlist:
        # print '+', cnt, vendor1_video.title
        video_qs = session.query(VideoRaw) \
            .filter(VideoRaw.vendor_url != 'ivi.ru')
        if (vendor1_video.original_title):
            video_qs = video_qs.filter(
                VideoRaw.original_title == vendor1_video.original_title
            )
        else:
            print 'no o', cnt, vendor1_video.title
            video_qs = video_qs.filter(VideoRaw.title == vendor1_video.title)
        if (vendor1_video.episode):
            video_qs = video_qs.filter(
                VideoRaw.episode == vendor1_video.episode
            )
        video_list = video_qs.all()
        if (video_list):
            cnt += 1
            for video in video_list:
                print "\n\n%s, [%s] %s (%s)\n[%s]\n%s\n\n" % (
                cnt, vendor1_video.title, video.title, video.vendor_url, vendor1_video.description, video.description)
    print cnt
    # for vendor2_video in vendor2_vlist:
    # for megogog_video in megogo_vlist:
    # if (
    # (vendor1_video.title == vendor2_video.title == megogog_video.title)
    # and (vendor1_video.original_title == vendor2_video.original_title)
    ##or (tvzavr_video.duration == vendor2_video.duration)
    # ):
    # print "%s «%s» -> «%s», (%s, %s)"%(cnt, vendor1_video.title, vendor2_video.title,vendor1_video.id, vendor2_video.id,)
    # vendor_cnt += 1
    # print cnt



def fetch_vendor(vendor, session=None, video_id_list=[]):
    if not session:
        scoped_session = orm.get_scoped_session()
        local_session = scoped_session()
    else:
        local_session = session
    vlist = local_session.query(VideoRaw) \
        .filter(VideoRaw.vendor_url == vendor) \
        .order_by(VideoRaw.title) \
        .all()
    if not session:
        local_session.close()
    return [(vendor, vlist)]


def mine_normalized_video():
    CUR_VERSION = 6

    session = orm.get_scoped_session()
    vendor_list = [
        'now.ru',
        'tvzavr.ru',
        'ivi.ru',
        'tvzavr.ru',
        'tvigle.ru',
        'megogo.net',
    ]

    tvzavr_video_id_list = session.query(VideoRaw.id) \
        .filter(VideoRaw.vendor_url == 'tvzavr.ru') \
        .order_by(VideoRaw.title) \
        .all()

    tvzavr_video_id_list = [vid[0] for vid in tvzavr_video_id_list]

    video_id_list = [
        video_id[0]
        for video_id in session.query(
            VideoToVideo.video_id,
            VideoToVideo.copy_video_id
        ).filter(
            ~VideoToVideo.copy_video_id.in_(tvzavr_video_id_list)
        ).filter(
            VideoToVideo.version < CUR_VERSION
        ).all()
    ]

    video_dict = {}
    for vendor in vendor_list:
        video_dict.update(dict(fetch_vendor(vendor, session, video_id_list)))

    res = [
        handle_vendor(video_dict, vendor1, vendor2, video_id_list)
        for v1, vendor1 in enumerate(vendor_list)
        for v2, vendor2 in enumerate(vendor_list) if (v1 < v2)
    ]


def filter_video(video):
    for vi in video.copy_list:
        if vi.vendor_url == 'tvzavr.ru':
            return False

    return video.description and (len(video.description) > 140)


def handle_vendor(video_dict, vendor1, vendor2, video_id_list):
    start_time = time.time()
    sys.stderr.write("(2)start_time = %s\n" % (start_time))

    video_list1 = video_dict[vendor1]
    video_list2 = video_dict[vendor2]

    video_list1 = filter(filter_video, video_list1)
    video_list2 = filter(filter_video, video_list2)

    production = itertools.product(video_list1, video_list2)

    video_result_list = []

    # video_gevent_pool = Pool(1000)
    # video_dummy_pool = multiprocessing.dummy.Pool(100)

    islice = list(itertools.islice(production, 1000))

    while (islice):
        for video1, video2 in islice:
            if ((video1.id, video2.id) in video_id_list):
                continue
            handle_video((video1, video2), )

            # video_map_obj = video_dummy_pool.map_async(
            # handle_video,
            # islice,
            ##callback=video_result_list.append
        # )
        # video_map_obj.get(1e100)

        # video_gevent_pool.map(handle_video, islice)

        # [handle_video((video1, video2),) for video1, video2 in islice]

        islice = list(itertools.islice(production, 1000))

    # video_dummy_pool.close()
    # video_dummy_pool.join()

    stop_time = time.time()
    sys.stderr.write("(2)stop_time =  %s\n" % (stop_time))

    diff_time = stop_time - start_time
    sys.stderr.write("(2)diff_time =  %s\n" % (diff_time))


def handle_video((video1, video2), ):
    # pass
    # print 'x %s' %(thread.get_ident())


    original_title = False
    episode = False

    video1_striped_title = strip_title(video1.title)
    video2_striped_title = strip_title(video2.title)

    if (
                    video1.episode == video2.episode
            and video1.release_date == video2.release_date
    ):

        if (video1.original_title and video1.original_title == video2.original_title):
            sys.stdout.write('A ')
            merge_video((video1, video2), )

        elif (video1.title == video2.title):
            sys.stdout.write('B ')
            merge_video((video1, video2), )

        elif (video1_striped_title == video2_striped_title):
            sys.stdout.write('C ')
            merge_video((video1, video2), )

    elif (video1_striped_title == video2_striped_title):
        sys.stdout.write('D ')
        merge_video((video1, video2), )

    elif (video1.parent_title and video1.parent_title == video2.parent_title):
        if (video1.episode and video1.episode == video2.episode):
            sys.stdout.write('E ')
            merge_video((video1, video2), )

        elif (compare_serial_title(video1.title, video2.title)):
            sys.stdout.write('F ')
            merge_video((video1, video2), )


            # TODO: icontains


def merge_video((video1, video2), ):
    tm = time.ctime();
    sys.stdout.write("vi %s | %s | %s | «%s»  «%s» [%s] [%s]\n" % (
    tm, video1.id, video2.id, video1.title, video2.title, video1.vendor_url, video2.vendor_url))
    sys.stdout.flush()


def __strip_title(title):
    title = lower(title)
    title = re.sub("\(\d+\)", '', title)
    title = title.replace('-серия', '. серия')
    title = title.replace('-сезон', '. сезон')
    title = title.replace('-часть', '. часть')

    title = title.replace(':', '.')
    title = title.replace(',', '.')

    return title


def strip_title(title):
    title = __strip_title(title)
    title = title.replace('.', '')
    return title


def x_strip_title(title):
    title = __strip_title(title)
    title = title.replace('...', '')
    title = title.replace('..', '')
    title = re.sub('(\d+) (серия)', '\g<2> \g<1>', title)
    title = re.sub('(\d+) (сезон)', '\g<2> \g<1>', title)
    title = re.sub('(\d+) (часть)', '\g<2> \g<1>', title)

    title = re.sub('(часть) (\d+)', 'ч\g<2>', title)

    title = re.sub('(сезон \d+). (серия \d+)', '\g<1>\g<2>', title)
    return title


def compare_serial_title(title1, title2):
    title1 = x_strip_title(title1)
    title2 = x_strip_title(title2)

    title1_list = re.split(u'[\.\,\-\:\(\)]', title1)[1:]
    title2_list = re.split(u'[\.\,\-\:\(\)]', title2)[1:]

    for t1 in title1_list:
        for t2 in title2_list:
            if (len(t1) > 5 and t1 == t2):
                return True
    return False


def lower(string):
    return unicode(string, 'utf8').lower().encode('utf8')


mine_normalized_video()
