#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import logging
import pymorphy2
from vxa.models import Tag
from vxa.models import Orm
from vxa.models import TagType

from vxa.settings.logging import start_logging


class CategoryNormalizer(object):
    """
        Builds initial form of category tags
        and write it into data base
            «Cats» -> «cat»

        TODO:
        *   use Twisted + SqlAlchemy
            http://tony.su/2011/09/04/twisted-and-sqlalchemy/
        *   use mongodb with map reduce

    """

    __logger = logging.getLogger(__name__)

    TAG_TYPE_NAME_LIST = [
        'tag',
        'category',
        'genre'
    ]

    def __init__(self):
        self.orm = Orm()
        self.morph = pymorphy2.MorphAnalyzer()

    def normalize(self):
        self.__logger.debug("start normalize category")
        session = self.orm.get_session()
        tag_type_id_list = self.get_tag_types(session)
        query_set = session.query(Tag) \
            .filter(Tag.norm_tag_id == None) \
            .filter(Tag.tag_type_id.in_(tag_type_id_list))
        category_tag_list = self.filter_tag_list(query_set)
        category_tag_list = self.add_norm_name(category_tag_list)
        self.insert_new_tag_list(session, category_tag_list)
        query_set = query_set.filter(Tag.vendor_url == 'vxa')
        pivot_tag_list = self.filter_tag_list(query_set)
        self.link_to_pivot(session, category_tag_list, pivot_tag_list)
        session.close()
        self.__logger.debug("stop normalize category")

    def filter_tag_list(self, query_set):
        return [tag for tag in query_set.all() if (len(tag.name.split(' ')) < 2)]

    def link_to_pivot(self, session, category_tag_list, pivot_tag_list):
        self.__logger.debug("start link_to_pivot")
        for category_tag in category_tag_list:
            if category_tag.norm_name:
                for pivot_tag in pivot_tag_list:
                    if pivot_tag.name.decode('utf8') == category_tag.norm_name:
                        category_tag.norm_tag_id = pivot_tag.id
                        pivot_tag.norm_tag_id = pivot_tag.id
                        session.flush()
                        break
        self.__logger.debug("end link_to_pivot")

    def insert_new_tag_list(self, session, category_tag_list):
        self.__logger.debug("start insert_new_tag_list")
        new_tag_list = [
            Tag(
                name=category_tag.norm_name,
                vendor_url='vxa',
                tag_type_id=category_tag.tag_type_id,
            )
            for category_tag in category_tag_list if category_tag.norm_name
        ]
        Tag.bulk_insert(session, new_tag_list)
        session.flush()
        self.__logger.debug("end insert_new_tag_list")

    def add_norm_name(self, category_tag_list):
        for category_tag in category_tag_list:
            category_tag.norm_name = self.normalize_category_tag(category_tag)
        return category_tag_list

    def normalize_category_tag(self, category_tag):
        morph_obj = self.morph.parse(category_tag.name.decode('utf8'))
        norm_name = None
        if morph_obj:
            norm_name = morph_obj[0].normal_form
        return norm_name

    def get_tag_types(self, session):
        tag_type_list = session.query(TagType).all()
        tag_type_id_list = []
        for tag_type in tag_type_list:
            if tag_type.name in self.TAG_TYPE_NAME_LIST:
                tag_type_id_list += [tag_type.id]
        return tag_type_id_list


if __name__ == '__main__':
    start_logging()
    merger = CategoryNormalizer()
    merger.normalize()
