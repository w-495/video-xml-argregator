#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import re

import logging

from sqlalchemy import or_

from vxa.models import VideoRaw
from vxa.models import Orm

from vxa.settings.logging import start_logging


class SerialMerger(object):
    """
        TODO:
        *   use Twisted + SqlAlchemy
            http://tony.su/2011/09/04/twisted-and-sqlalchemy/
        *   use mongodb with map reduce

    """

    __logger = logging.getLogger(__name__)

    def __init__(self):
        self.orm = Orm()

    def merge(self):
        self.__logger.debug("start merge serials")
        scoped_session = self.orm.get_scoped_session()
        local_session = scoped_session()
        video_list = local_session.query(VideoRaw) \
            .filter(or_(VideoRaw.title.contains('. '), VideoRaw.title.contains(': '))) \
            .filter(VideoRaw.parent_title == None) \
            .all()
        for video in video_list:
            title_list = re.split(u'[\.\-\:\(\)]', video.title)
            if len(title_list) > 1:
                parent_title = title_list[0]
                parent_title = parent_title.replace('\xe2\x80\x9c', '')
                parent_title = parent_title.replace('\xe2\x80\x9d', '')
                video.parent_title = parent_title
                self.__logger.debug("%s => %s"%(parent_title, video.title))
                local_session.flush()
        local_session.close()
        self.__logger.debug("stop merge serials")

if __name__ == '__main__':
    start_logging()
    merger = SerialMerger()
    merger.merge()
