#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import logging

import itertools

from vxa.models import Tag
from vxa.models import TagType
from vxa.models import Orm

from vxa.settings.logging import start_logging

class PersonNormalizer(object):
    """
        Now.ru has tag.name = «Surname Firstname»
        and others hast tag.name = «Firstname Surname».
        We use now.ru tags as pivot.

        TODO:
        *   use Twisted + SqlAlchemy
            http://tony.su/2011/09/04/twisted-and-sqlalchemy/
        *   use mongodb with map reduce
    """
    
    PIVOT_VENDOR_URL = 'now.ru'

    TAG_TYPE_NAME_PIVOT = 'tag'

    TAG_TYPE_NAME_RIGHT = 'person'

    TAG_TYPE_NAME_LIST = [
        'person',
        'script',
        'actor',
        'director',
        'tag'
    ]

    __logger = logging.getLogger(__name__)

    def __init__(self):
        self.orm = Orm()

    def normalize(self):
        self.__logger.debug("start normalize person")
        session = self.orm.get_scoped_session()
        tag_type_id_pivot, tag_type_id_right, tag_type_id_list = self.get_tag_types(session)
        query_set = session.query(Tag).filter(Tag.norm_tag_id == None)
        pivot_category_tag_list = query_set \
            .filter(Tag.tag_type_id == tag_type_id_pivot) \
            .filter(Tag.vendor_url == self.PIVOT_VENDOR_URL) \
            .all()
        person_category_tag_list = query_set \
            .filter(Tag.tag_type_id.in_(tag_type_id_list)) \
            .filter(Tag.vendor_url != self.PIVOT_VENDOR_URL) \
            .all()
        for pivot_tag in pivot_category_tag_list:
            for person_tag in person_category_tag_list:
                tag_words = person_tag.name.split(' ')
                tag_perm_gen = itertools.permutations(tag_words)
                for i, tag_perm in enumerate(tag_perm_gen):
                    new_name = ' '.join(tag_perm)
                    if (person_tag.name != new_name) and (pivot_tag.name == new_name):
                        self.__logger.debug("%s %s => %s %s"%(
                                person_tag.id,
                                person_tag.name,
                                pivot_tag.id,
                                pivot_tag.name
                            )
                        )
                        pivot_tag.norm_tag_id = pivot_tag.id
                        pivot_tag.tag_type_id = tag_type_id_right
                        person_tag.norm_tag_id = pivot_tag.id
                        person_tag.tag_type_id = person_tag.tag_type_id
                        session.flush()
                        break
        session.close()
        self.__logger.debug("stop normalize person")

    def get_tag_types(self, session):
        tag_type_list = session.query(TagType).all()
        tag_type_id_pivot = None
        tag_type_id_right = None
        tag_type_id_list = []
        for tag_type in tag_type_list:
            if tag_type.name == self.TAG_TYPE_NAME_PIVOT:
                tag_type_id_pivot = tag_type.id
            if tag_type.name == self.TAG_TYPE_NAME_RIGHT:
                tag_type_id_right = tag_type.id
            if tag_type.name in self.TAG_TYPE_NAME_LIST:
                tag_type_id_list += [tag_type.id]
        return tag_type_id_pivot, tag_type_id_right, tag_type_id_list


if __name__ == '__main__':
    start_logging()
    merger = PersonNormalizer()
    merger.normalize()
