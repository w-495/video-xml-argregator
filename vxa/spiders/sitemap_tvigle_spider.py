# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

from .lib.sitemap_base_spider_yandex_video import SitemapBaseSpiderYandexVideo


class SitemapTvigleSpider(SitemapBaseSpiderYandexVideo):
    """
        Hint:
            For http://video.yandex.ru/schemas/video_import
            use proxy to http://webmaster.yandex.ru/schemas/video.
            Simplest way to make proxy — set up your oun nginx server with such config:
                server {
                    listen 80 ;
                    server_name video.yandex.ru www.video.yandex.ru;
                    location /schemas/video_import {
                         access_log  /var/log/nginx/video.yandex.ru.access.log  main;
                         error_log   /var/log/nginx/video.yandex.ru.error.log info;
                         proxy_redirect off;
                         proxy_pass                      https://webmaster.yandex.ru/schemas/video;
                         proxy_set_header                Host video.yandex.ru ;
                    }
                }
    """

    name = 'tvigle.ru'

    allowed_domains = {
        'www.tvigle.ru',
        'tvigle.ru',
    }

    start_urls = (
        'http://www.tvigle.ru/seo/sitemap_catalog.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_1.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_2.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_3.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_4.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_5.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_6.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_7.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_8.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_9.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_10.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_11.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_12.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_13.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_14.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_15.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_16.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_17.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_18.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_19.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_20.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_21.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_22.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_23.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_24.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_25.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_26.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_27.xml',
        'http://www.tvigle.ru/seo/sitemap_videos_28.xml',
    )

    namespaces = dict(
        ovs='http://webmaster.yandex.ru/schemas/video',
        yvs='http://video.yandex.ru/schemas/video_import',
        sm='http://www.sitemaps.org/schemas/sitemap/0.9'
    )

    video_item_xpath = '//ovs:video'

    video_list_xpath = '//yvs:video/yvs:feed'
