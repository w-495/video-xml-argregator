# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

from .lib.sitemap_base_spider_yandex_video import SitemapBaseSpiderYandexVideo


class SitemapIviruSpider(SitemapBaseSpiderYandexVideo):

    name = 'ivi.ru'

    allowed_domains = {
        'ivi.ru',
        'www.ivi.ru'
    }

    start_urls = (
        'http://www.ivi.ru/sitemap_series_detective.xml',
        'http://www.ivi.ru/sitemap_series_sciencefiction.xml',
        'http://www.ivi.ru/sitemap_series_poznavatelnye.xml',
        'http://www.ivi.ru/sitemap_series_dramy.xml',
        'http://www.ivi.ru/sitemap_series_filmyi.xml',
        'http://www.ivi.ru/sitemap_series_istoricheskie.xml',
        'http://www.ivi.ru/sitemap_series_melodramyi.xml',
        'http://www.ivi.ru/sitemap_series_adventures.xml',
        'http://www.ivi.ru/sitemap_series_romanticheskie.xml',
        'http://www.ivi.ru/sitemap_series_zhivaya_priroda.xml',
        'http://www.ivi.ru/sitemap_series_voennye.xml',
        'http://www.ivi.ru/sitemap_series_comedy.xml',
        'http://www.ivi.ru/sitemap_series_documentary.xml',
        'http://www.ivi.ru/sitemap_series_boeviki.xml',
        'http://www.ivi.ru/sitemap_series_thriller.xml',
        'http://www.ivi.ru/sitemap_animation_polnometrazhnye.xml',
        'http://www.ivi.ru/sitemap_animation_russkie_multfilmyi.xml',
        'http://www.ivi.ru/sitemap_animation_pro_zhivotnyih.xml',
        'http://www.ivi.ru/sitemap_animation_vzroslye.xml',
        'http://www.ivi.ru/sitemap_animation_detskie.xml',
        'http://www.ivi.ru/sitemap_animation_skazki.xml',
        'http://www.ivi.ru/sitemap_animation_zapadnyie_multfilmyi.xml',
        'http://www.ivi.ru/sitemap_animation_serialy.xml',
        'http://www.ivi.ru/sitemap_animation_sovetskie.xml',
        'http://www.ivi.ru/sitemap_animation_anime.xml',
        'http://www.ivi.ru/sitemap_animation_anime_deti.xml',
        'http://www.ivi.ru/sitemap_animation_hochu_vsyo_znat.xml',
        'http://www.ivi.ru/sitemap_animation_detskie_pesni.xml',
        'http://www.ivi.ru/sitemap_animation_filmyi.xml',
        'http://www.ivi.ru/sitemap_ne_opredeleno_nikakoj.xml',
        'http://www.ivi.ru/sitemap_programs_wellness.xml',
        'http://www.ivi.ru/sitemap_programs_hochu_vsyo_znat.xml',
        'http://www.ivi.ru/sitemap_programs_poznavatelnye.xml',
        'http://www.ivi.ru/sitemap_programs_hobby.xml',
        'http://www.ivi.ru/sitemap_movies_detective.xml',
        'http://www.ivi.ru/sitemap_movies_poznavatelnye.xml',
        'http://www.ivi.ru/sitemap_movies_filmyi.xml',
        'http://www.ivi.ru/sitemap_movies_skazki.xml',
        'http://www.ivi.ru/sitemap_movies_fentezi.xml',
        'http://www.ivi.ru/sitemap_movies_vokrug-sveta.xml',
        'http://www.ivi.ru/sitemap_movies_vzroslye.xml',
        'http://www.ivi.ru/sitemap_movies_fantastika.xml',
        'http://www.ivi.ru/sitemap_movies_zhivaya_priroda.xml',
        'http://www.ivi.ru/sitemap_movies_detskiy.xml',
        'http://www.ivi.ru/sitemap_movies_erotika.xml',
        'http://www.ivi.ru/sitemap_movies_comedy.xml',
        'http://www.ivi.ru/sitemap_movies_detskie_pesni.xml',
        'http://www.ivi.ru/sitemap_movies_boeviki.xml',
        'http://www.ivi.ru/sitemap_movies_dlya_vsej_semi.xml',
        'http://www.ivi.ru/sitemap_movies_horror.xml',
        'http://www.ivi.ru/sitemap_movies_istoricheskie.xml',
        'http://www.ivi.ru/sitemap_movies_voennye.xml',
        'http://www.ivi.ru/sitemap_movies_pro_zhivotnyih.xml',
        'http://www.ivi.ru/sitemap_movies_thriller.xml',
        'http://www.ivi.ru/sitemap_movies_nemoe_kino.xml',
        'http://www.ivi.ru/sitemap_movies_arthouse.xml',
        'http://www.ivi.ru/sitemap_movies_korotkometrazhki.xml',
        'http://www.ivi.ru/sitemap_movies_russkie_multfilmyi.xml',
        'http://www.ivi.ru/sitemap_movies_adventures.xml',
        'http://www.ivi.ru/sitemap_movies_muzyikalnyie.xml',
        'http://www.ivi.ru/sitemap_movies_drama.xml',
        'http://www.ivi.ru/sitemap_movies_melodramy.xml',
        'http://www.ivi.ru/sitemap_movies_sovetskoe_kino.xml',
        'http://www.ivi.ru/sitemap_movies_documentary.xml',
    )

    namespaces = dict(
        ovs='http://webmaster.yandex.ru/schemas/video',
        yvs='http://video.yandex.ru/schemas/video_import'
    )

    video_item_xpath = '//ovs:video'

    video_list_xpath = '//yvs:video/yvs:feed'

