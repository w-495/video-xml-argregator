# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

from .lib.sitemap_base_spider_google_video import SitemapBaseSpiderGoogleVideo


class SitemapNowruSpider(SitemapBaseSpiderGoogleVideo):

    name = 'now.ru'

    allowed_domains = {
        'now.ru'
    }

    start_urls = (
        'http://www.now.ru/sitemaps/free.xml',
    )

    _namespaces = dict(
        video='http://www.google.com/schemas/sitemap-video/1.1',
        sm='http://www.sitemaps.org/schemas/sitemap/0.9'
    )



