# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import re

from dateutil import parser as dt_parser


from scrapy import Item, Field

from .utils import tolong


class StringField(Field):

    REPLACE_RE_LIST = [
        (r'"(.*?)"', u'«\\1»'),
        (r"'(.*?)'", u'«\\1»'),
        (r"'<strong>.*?</strong>'", u'‹\\1›'),
        (r"'<em>.*?</em>'", u'‹\\1›'),
        (r"'<b>.*?</b>'", u'‹\\1›'),
        (r"'<i>.*?</i>'", u'‹\\1›'),
        (r'<br/>', u'\n\n',),
        (r'<[^>]+>', u'',),
    ]

    REPLACE_CHAR_LIST = [
        ("'", u"‛"),
        ('"', u"‟"),
        ('<', u"‹"),
        ('>', u"›"),
    ]

    def __init__(self):
        super(StringField, self).__init__(serializer=self.serializer)

    def serializer(self, string):
        """
            Returns Unicode String
        """
        if string:
            string = self.escape(u"%s" % (string))
            return string
        return string

    def escape(self, string):
        for pattern, replacement in self.REPLACE_RE_LIST:
            string = re.sub(
                pattern=pattern,
                repl=replacement,
                string=string,
                flags=re.UNICODE | re.MULTILINE | re.DOTALL
            )
        for pattern, replacement in self.REPLACE_CHAR_LIST:
            string = re.sub(
                pattern=pattern,
                repl=replacement,
                string=string,
                flags=re.UNICODE | re.MULTILINE | re.DOTALL
            )
        return string



class DateField(Field):
    def __init__(self):
        super(DateField, self).__init__(serializer=self.serializer)

    def serializer(self, string):
        if string:
            try:
                dt = dt_parser.parse(u"%s" % (string))
                dt_list = dt.isoformat(' ').split('+')[:1]
                if (dt_list):
                    string = dt_list[0]
            except:
                string = None
        return string


class LongField(Field):
    def __init__(self):
        super(LongField, self).__init__(serializer=tolong)


class TagItem(Item):
    name = StringField()
    tag_type_name = StringField()
    attr_dict = Field()
    vendor_url = StringField()


class VideoItem(Item):
    title = StringField()

    content_id = LongField()
    duration = LongField()

    parent_title = StringField()
    original_title = StringField()
    description = StringField()

    season = LongField()
    episode = LongField()
    part = LongField()

    create_date = DateField()
    modify_date = DateField()
    expiration_date = DateField()
    release_date = DateField()
    start_date = DateField()
    end_date = DateField()
    upload_date = DateField()

    views_last_day = LongField()
    views_last_month = LongField()
    views_last_week = LongField()
    views_total = LongField()
    comments = LongField()
    likes = LongField()
    dislikes = LongField()
    rating = LongField()
    rating_count = LongField()

    page_url = StringField()
    player_url = StringField()
    content_url = StringField()
    thumbnail_url = StringField()
    poster_url = StringField()
    feed_url = StringField()
    gallery_url = StringField()
    vendor_url = StringField()
    source_url = StringField()

    embed_html = StringField()

    tag_list = Field()

    unknown_field_dict = Field()
