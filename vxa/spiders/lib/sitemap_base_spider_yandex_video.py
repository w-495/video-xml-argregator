# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

from .sitemap_base_spider import SitemapBaseSpider


class SitemapBaseSpiderYandexVideo(SitemapBaseSpider):

    allowed_domains = {
        "ivi.ru"
    }

    start_urls = (
        'http://www.ivi.ru/export/yandex/feed/?id=51072',
    )

    _namespaces = dict(
        ovsold='http://video.yandex.ru/schemas/video_import',
        ovs='http://webmaster.yandex.ru/schemas/video',
        sm='http://www.sitemaps.org/schemas/sitemap/0.9'
    )

    _video_field_list = (
        (
            'ovs:title',
            'title'
        ),
        (
            'ovs:content_id',
            'content_id'
        ),
        (
            'ovs:url',
            'page_url'
        ),
        (
            'ovs:thumbnail',
            'thumbnail_url'
        ),
        (
            'ovs:movie/ovs:original_name',
            '?original_title'
        ),
        (
            'ovs:movie/ovs:release_date',
            '?release_date'
        ),
        (
            'ovs:movie/ovs:part',
            'part'
        ),
        (
            'ovs:tvseries/ovs:original_name',
            '?original_title'
        ),
        (
            'ovs:tvseries/ovs:release_date',
            '?release_date'
        ),
        (
            'ovs:tvseries/ovs:season',
            '?season'
        ),
        (
            'ovs:tvseries/ovs:episode',
            '?episode'
        ),
        (
            'ovs:tvshow/ovs:original_name',
            '?original_title'
        ),
        (
            'ovs:tvshow/ovs:release_date',
            '?release_date'
        ),
        (
            'ovs:tvshow/ovs:season',
            '?season'
        ),
        (
            'ovs:tvshow/ovs:episode',
            '?episode'
        ),
        (
            'ovs:broadcast/ovs:start_date',
            'start_date'
        ),
        (
            'ovs:broadcast/ovs:end_date',
            'end_date'
        ),
        (
            'ovs:music/ovs:track_name',
            '?original_title'
        ),
        (
            'ovs:music/ovs:album',
            'parent_title'
        ),
        (
            'ovs:duration',
            'duration'
        ),
        (
            'ovs:description',
            'description'
        ),
        (
            'ovs:stats/views_last_day',
            'views_last_day'
        ),
        (
            'ovs:stats/views_last_month',
            'views_last_month'
        ),
        (
            'ovs:stats/views_last_week',
            'views_last_week'
        ),
        (
            'ovs:stats/views_total',
            'views_total'
        ),
        (
            'ovs:stats/comments',
            'comments'
        ),
        (
            'ovs:stats/likes',
            'likes'
        ),
        (
            'ovs:stats/dislikes',
            'dislikes'
        ),
        (
            'ovs:stats/rating',
            'rating'
        ),
        (
            'ovs:stats/rating_count',
            'rating_count'
        ),
        (
            'ovs:content_url',
            'content_url'
        ),
        (
            'ovs:embed_html',
            'embed_html'
        ),
        (
            'ovs:embed_url',
            'player_url'
        ),
        (
            'ovs:feed',
            'feed_url'
        ),
        (
            'ovs:poster',
            'poster_url'
        ),
        (
            'ovs:create_date',
            'create_date'
        ),
        (
            'ovs:upload_date',
            'upload_date'
        ),
        (
            'ovs:modify_date',
            'modify_date'
        ),
        (
            'ovs:expiration_date',
            'expiration_date'
        ),

        (
            ['ovs:status'],
            dict(
                type='status_type'
            )
        ),
        (
            ['ovs:category'],
            dict(
                type='category'
            )
        ),
        (
            ['ovs:genre'],
            dict(
                type='genre'
            )
        ),
        (
            ['ovs:tag'],
            dict(
                type='tag'
            )
        ),
        (
            ['ovs:login'],
            dict(
                type='login'
            )
        ),
        (
            ['ovs:country'],
            dict(
                type='country'
            )
        ),
        (
            ['ovs:subtitle'],
            dict(
                type='subtitle'
            )
        ),
        (
            ['ovs:license'],
            dict(
                type='license_type'
            )
        ),
        (
            ['ovs:dubbing'],
            dict(
                type='dubbing'
            )
        ),
        (
            ['ovs:languages'],
            dict(
                type='language'
            )
        ),
        (
            ['ovs:available_platform'],
            dict(
                type='platform_type'
            )
        ),
        (
            ['ovs:allow_countries'],
            dict(
                type='allow_country'
            )
        ),
        (
            ['ovs:disallow_countries'],
            dict(
                type='disallow_country'
            )
        ),
        (
            ['ovs:production_company'],
            dict(
                type='production_company'
            )
        ),
        (
            ['ovs:price'],
            dict(
                type='price'
            )
        ),
        (
            ['ovs:available_format/ovs:format'],
            dict(
                type='available_format'
            )
        ),
        (
            ["ovs:person[@role='director']"],
            dict(
                type='director'
            )
        ),
        (
            ["ovs:person[@role='film director']"],
            dict(
                type='director'
            )
        ),
        (
            ["ovs:person[@role='actor']"],
            dict(
                type='actor'
            )
        ),
        (
            ["ovs:person[@role='script']"],
            dict(
                type='script'
            )
        ),
        (
            ['ovs:available_platform'],
            dict(
                type='platform_type',
                splitter=' '
            )
        ),
        (
            ['ovs:is_official'],
            dict(
                type='is_official_type',
                map=dict(
                    yes='official',
                    no='unofficial'
                )
            )
        ),
        (
            ['ovs:allow_embed'],
            dict(
                type='allow_embed_type',
                map=dict(
                    yes='allow_embed',
                    no='disallow_embed'
                )
            )
        ),
        (
            ['ovs:adult'],
            dict(
                type='adult_type',
                map=dict(
                    yes='adult',
                    no='family_friendly'
                )
            )
        ),
    )

    video_item_xpath = '//ovs:video'

    video_list_xpath = '//ovsold:video/ovsold:feed/text()'

    def parse_url_node(self, response, video_node):
        return self.parse_video_node(response, video_node)
