# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

import re
import six

from scrapy import Request, Selector

from scrapy.spiders import XMLFeedSpider

from scrapy.utils.spider import iterate_spider_output

from .utils import SmartString, car

from .items import TagItem, VideoItem


class SitemapBaseSpider(XMLFeedSpider):

    name = 'sitemap'

    allowed_domains = []
    start_urls = []

    _namespaces = dict(
        sm='http://www.sitemaps.org/schemas/sitemap/0.9'
    )

    _video_node_field_list = []

    video_item_xpath = '//sm:url'

    video_list_xpath = '//sm:sitemap/sm:loc'

    @property
    def namespaces(self):
        return self._namespaces

    @property
    def video_node_field_list(self):
        return self._video_node_field_list

    def selector_dom(self, response):
        return self.register_namespaces(Selector(response))

    def register_namespaces(self, dom):
        for namespace, schema in self.namespaces.iteritems():
            dom.register_namespace(namespace, schema)
        return dom

    def parse(self, response):
        dom = self.selector_dom(response)
        for url_node in dom.xpath(self.video_item_xpath):
            ret = iterate_spider_output(self.parse_url_node(response, url_node))
            for result_item in self.process_results(response, ret):
                yield result_item
        for url in dom.xpath("%s/text()" % (self.video_list_xpath)).extract():
            yield Request(url, callback=self.parse)

    def parse_url_node(self, response, url_node):
        return self.parse_video_node(response, url_node)

    def parse_video_node(self, response, video_node, page_url=None):
        video_item = self.build_video_item(
            tag_list=[],
            page_url=page_url,
            source_url=response.url,
            vendor_url=self.name
        )
        self.add_allowed_domains(video_item)
        for video_node_field, video_model_field in self.video_node_field_list:
            if isinstance(video_node_field, str):
                video_item = self.handle_single_fields(
                    video_node,
                    video_item,
                    video_node_field,
                    video_model_field,
                )
            elif isinstance(video_node_field, list):
                video_item = self.handle_multiple_fields(
                    video_node,
                    video_item,
                    video_node_field,
                    video_model_field,
                )

        return video_item

    def build_video_item(self, *args, **kwargs):
        video_item = VideoItem(*args, **kwargs)
        return video_item

    def add_allowed_domains(self, video_item):
        for allowed_domain in self.allowed_domains:
            video_item['tag_list'] += [
                self.build_tag_item(
                    vendor_url=self.name,
                    tag_type_name='vendor_type',
                    name=allowed_domain,
                )
            ]
        return video_item

    def build_tag_item(self, *args, **kwargs):
        tag_item = TagItem(*args, **kwargs)
        return tag_item

    def handle_single_fields(self, video_node, video_item, video_node_field, video_model_field):
        field_value = self.parse_video_node_field(video_node, video_node_field)
        if isinstance(video_model_field, str):
            if video_model_field.startswith('?'):
                field_name = video_model_field[1:]
                if video_item.get(field_name) is None:
                    video_item[field_name] = field_value
            else:
                video_item[video_model_field] = field_value
        if isinstance(video_model_field, set):
            field_name = list(video_model_field)[0]
            video_item.setdefault(field_name, {}).update({video_node_field: field_value})
        return video_item

    def handle_multiple_fields(self, video_node, video_item, video_node_field, video_model_field):
        field_value_list = self.parse_video_node_field_list(video_node, video_node_field[0])
        if isinstance(video_model_field, dict):
            field_value_list = self.handle_splitter(field_value_list, video_model_field)
            for field_value in field_value_list:
                attr_dict = vars(field_value)
                tag_item = self.build_tag_item(
                    vendor_url=self.name,
                    attr_dict=attr_dict,
                    tag_type_name=video_model_field.get('type'),
                )
                field_value = self.handle_attr_mapping(field_value, video_model_field, attr_dict)
                field_value = self.handle_common_mapping(field_value, video_model_field)
                tag_item['name'] = field_value
                video_item['tag_list'] += [tag_item]
        return video_item

    @staticmethod
    def handle_splitter(field_value_list, video_model_field, video_model_field_key='splitter'):
        splitter = video_model_field.get(video_model_field_key, ', ')
        if splitter:
            splitted_field_value_list = []
            for field_value in field_value_list:
                splitted_field_value_list += [
                    SmartString(part).copy_vars(field_value)
                    for part in re.split(splitter, field_value) if part
                ]
            field_value_list = splitted_field_value_list
        return field_value_list

    @staticmethod
    def handle_attr_mapping(field_value, video_model_field, attr_dict, video_model_field_key='attr_map'):
        attr_mapping = video_model_field.get(video_model_field_key)
        if attr_mapping:
            field_value = None
            for key, attr_video_model_field in six.iteritems(attr_mapping):
                raw_value = attr_dict.get(key)
                field_value = attr_video_model_field.get(raw_value)
        return field_value

    @staticmethod
    def handle_common_mapping(field_value, video_model_field, video_model_field_key='map'):
        mapping = video_model_field.get(video_model_field_key)
        if mapping:
            field_value = mapping.get(field_value)
        return field_value

    def parse_video_node_field(self, video_node, item):
        res = car(self.parse_node_field_list(video_node, item))
        return res

    def parse_video_node_field_list(self, video_node, item):
        field_list = self.parse_node_field_list(video_node, item)
        parsed_field_list = []
        for field in field_list:
            parsed_field_list += [
                SmartString(x).copy_vars(field) for x in re.split(', ', field)
            ]
        return parsed_field_list

    @staticmethod
    def parse_node_field_list(dom, item):
        dom_item = dom.xpath('./%s' % (item))
        res_list = []
        for prop in dom_item:
            res = SmartString(car(prop.xpath('string(.)').extract()))
            for position, attribute in enumerate(prop.xpath('@*'), start=1):
                name = car(prop.xpath('name(@*[%d])' % position).extract())
                value = attribute.extract()
                setattr(res, name, value)
            res_list += [res]
        return res_list
