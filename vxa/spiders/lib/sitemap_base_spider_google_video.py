# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

from .sitemap_base_spider import SitemapBaseSpider


class SitemapBaseSpiderGoogleVideo(SitemapBaseSpider):

    _namespaces = dict(
        video='http://www.google.com/schemas/sitemap-video/1.1',
        sm='http://www.sitemaps.org/schemas/sitemap/0.9'
    )

    video_item_xpath = '//sm:url'

    video_list_xpath = '//sm:sitemap/sm:loc/'

    _video_field_list = (
        (
            'video:thumbnail_loc',
            'thumbnail_url'
        ),
        (
            'video:title',
            'title'
        ),
        (
            'video:tvshow/video:show_title',
            'parent_title'
        ),
        (
            'video:tvshow/video:episode_title',
            'original_title'
        ),
        (
            'video:tvshow/video:premier_date',
            'release_date'
        ),
        (
            'video:tvshow/video:season_number',
            'season'
        ),
        (
            'video:tvshow/video:episode_number',
            'episode'
        ),
        (
            'video:description',
            'description'
        ),
        (
            'video:player_loc',
            'player_url'
        ),
        (
            'video:content_loc',
            '?content_url'
            # Sign «?» means that it is an optional field.
            # The value can be got from another key.
            # In this way «content_url» can be got from
            # «video:content_loc» and «video:content_segment_loc».
            # The second one matches if only first is not exists.
        ),
        (
            'video:content_segment_loc',
            '?content_url'
            # Sign «?» means that it is an optional field.
            # The value can be got from another key.
            # In this way «content_url» can be got from
            # «video:content_loc» and «video:content_segment_loc».
            # The second one matches if only first is not exists.
        ),
        (
            'video:duration',
            'duration'
        ),
        (
            'video:view_count',
            'views_total'
        ),
        (
            'video:rating',
            'rating'
        ),
        (
            'video:publication_date',
            'release_date'
        ),
        (
            'video:expiration_date',
            'expiration_date'
        ),
        (
            'video:gallery_loc',
            'gallery_url'
        ),

        (  # «[...]» means that it can be multiple values of this key.
           ['video:uploader'],
           dict(
               type='login'
           )
           ),
        (
            ['video:category'],
            dict(
                type='category'
            )
        ),
        (
            ['video:genre'],
            dict(
                type='genre'
            )
        ),
        (
            ['video:tag'],
            dict(
                type='tag'
            )
        ),
        (
            ['video:tvshow/video:video_type'],
            dict(
                type='tag'
            )
        ),
        (
            ['video:gallery_loc/@title'],
            dict(
                type='tag'
            )
        ),
        (
            ['video:id'],
            dict(
                type='tag'
            )
        ),
        (
            ['video:price'],
            dict(
                type='tag'
            )
        ),
        (
            ["video:restriction[@relationship='allow']"],
            dict(
                type='allow_country',
                splitter=' '
            )
        ),
        (
            ["video:restriction[@relationship='deny']"],
            dict(
                type='disallow_country',
                splitter=' '
            )
        ),
        (
            ["video:platform[@relationship='allow']"],
            dict(
                type='platform_type',
                splitter=' '
            )
        ),
        (
            ["video:platform[@relationship='allow']"],
            dict(
                type='allow_platform',
                splitter=' '
            )
        ),
        (
            ["video:platform[@relationship='deny']"],
            dict(
                type='disallow_platform',
                splitter=' '
            )
        ),
        (
            ['video:requires_subscription'],
            dict(
                type='subscription_type',
                map=dict(
                    yes='subscription_required',
                    no='subscription_not_required'
                )
            )
        ),
        (
            ['video:family_friendly'],
            dict(
                type='adult_type',
                map=dict(
                    yes='family_friendly',
                    no='adult',
                )
            )
        ),
        (
            ['video:live'],
            dict(
                type='genre',
                map=dict(
                    yes='live_broadcast',
                    no='live_no_broadcast'
                )
            )
        ),
        (
            ["video:player_loc"],
            dict(
                type='allow_embed_type',
                attr_map=dict(
                    allow_embed=dict(
                        yes='allow_embed',
                        no='disallow_embed'
                    )
                )
            )
        ),
    )

    def parse_url_node(self, response, url_node):
        loc = url_node.xpath('./sm:loc/text()').extract()
        video_node = url_node.xpath('./video:video')
        return self.parse_video_node(response, video_node, loc)
