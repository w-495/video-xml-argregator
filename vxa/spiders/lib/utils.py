# -*- coding: utf8 -*-

from __future__ import absolute_import


import hashlib
import copy
import six

def car(lst):
    return (lst or [None])[0]


def unique(a):
    seen = set()
    return [seen.add(x) or x for x in a if x not in seen]


class SmartString(unicode):
    def copy_vars(self, smart, *args, **kwargs):
        if None != smart:
            for name, value in six.iteritems(vars(smart)):
                setattr(self, name, value)
        return self


class SmartDict(dict):
    def __init__(self, *args, **kwargs):
        self.__dict__ = kwargs
        super(SmartDict, self).__init__(*args, **kwargs)


def toint(a, default = None):
    if(None == a):
        return default
    if isinstance(a, int):
        return a
    if isinstance(a, str) or isinstance(a, unicode):
        if(not check_isdigit(a)):
            return default
    try:
        return int(a)
    except:
        return default


def tolong(a, default = None):
    if(None == a):
        return default
    if isinstance(a, long):
        return a
    if isinstance(a, str) or isinstance(a, unicode):
        if(not check_isdigit(a)):
            return default
    try:
        return long(a)
    except:
        return default



def check_isdigit(s):
    if len(s) and s[0] in ('-', '+'):
        return s[1:].isdigit()
    return s.isdigit()

sstr = SmartString
