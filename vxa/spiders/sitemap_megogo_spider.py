# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

from .lib.sitemap_base_spider_google_video import SitemapBaseSpiderGoogleVideo


class SitemapMegogoSpider(SitemapBaseSpiderGoogleVideo):

    name = 'megogo.net'

    allowed_domains = {
        'megogo.net'
    }
    
    start_urls = (
        'http://megogo.net/s/data/sitemap/sitemap.xml'
    )

    namespaces = dict(
        video='http://www.google.com/schemas/sitemap-video/1.0',
        sm='http://www.sitemaps.org/schemas/sitemap/0.9'
    )


