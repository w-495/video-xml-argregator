# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

from .lib.sitemap_base_spider_yandex_video import SitemapBaseSpiderYandexVideo


class SitemapTvzavrSpider(SitemapBaseSpiderYandexVideo):
    """
        Hint:
            For http://video.yandex.ru/schemas/video_import
            use proxy to http://webmaster.yandex.ru/schemas/video.
            Simplest way to make proxy — set up your oun nginx server with such config:
                server {
                    listen 80 ;
                    server_name video.yandex.ru www.video.yandex.ru;
                    location /schemas/video_import {
                         access_log  /var/log/nginx/video.yandex.ru.access.log  main;
                         error_log   /var/log/nginx/video.yandex.ru.error.log info;
                         proxy_redirect off;
                         proxy_pass                      https://webmaster.yandex.ru/schemas/video;
                         proxy_set_header                Host video.yandex.ru ;
                    }
                }
    """
    name = 'tvzavr.ru'

    allowed_domains = {
        'www.tvzavr.ru',
        'tvzavr.ru',
    }

    start_urls = (
        'http://www.tvzavr.ru/sitemap.xml',
    )

    namespaces = dict(
        ovs='http://webmaster.yandex.ru/schemas/video',
        yvs='http://video.yandex.ru/schemas/video_import',
        sm='http://www.sitemaps.org/schemas/sitemap/0.9'
    )

    video_item_xpath = '//ovs:video'

    video_list_xpath = '//sm:url/yvs:video/yvs:feed'

