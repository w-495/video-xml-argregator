#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from sqlalchemy.types import BINARY, VARCHAR, TIMESTAMP

from sqlalchemy.dialects.mysql import BIGINT, TEXT, DATETIME

class String(VARCHAR):
    def __init__(self, *args, **kwargs):
        super(String, self).__init__(
            length = 255,
        )


class StringUrl(VARCHAR):
    def __init__(self, *args, **kwargs):
        super(StringUrl, self).__init__(
            length = 255,
        )


class StringLong(TEXT):
    def __init__(self, *args, **kwargs):
        super(StringLong, self).__init__()


class StringHash(BINARY):
    def __init__(self, *args, **kwargs):
        super(StringHash, self).__init__(
            length = 32,
        )


class IntegerLong(BIGINT):
    def __init__(self, *args, **kwargs):
        super(IntegerLong, self).__init__(
            display_width=20,
            unsigned = True,
        )


class Timestamp(TIMESTAMP):
    def __init__(self, *args, **kwargs):
        super(Timestamp, self).__init__()



class DateTime(DATETIME):
    def __init__(self, *args, **kwargs):
        super(DateTime, self).__init__()

