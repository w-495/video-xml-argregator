#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import hashlib

from .unique_hash_model_mixin import UniqueHashModelMixin


class Md5HashModelMixin(UniqueHashModelMixin):

    @classmethod
    def get_hash_by_string(cls, string):
        return hashlib.md5(string.encode('utf-8')).hexdigest()

    @classmethod
    def prev_insert(cls, session):
        pass
