#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from ..utils import SmartDict

from .md5_hash_model_mixin import Md5HashModelMixin

from .column_types import ColumnHash


class MultiHashModelMixin(Md5HashModelMixin):

    unique_hash_rule = SmartDict(
        target_field_list=[],
        hash_field='hash_md5'
    )

    hash_md5 = ColumnHash()



