#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData


class VxaExtra(object):

    __table__ = None

    __table_args__ = dict(
        mysql_engine='MyISAM',
        mysql_charset='utf8'
    )

    def getdict(self):
        out = {}
        for column in self.__table__.columns:
            out[column.name] = getattr(self, column.name)
        return out

BaseModel = declarative_base(
    cls=VxaExtra,
    metadata=MetaData()
)

