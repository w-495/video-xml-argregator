#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import logging

from ..utils import unique, SmartDict

from .md5_hash_model_mixin import Md5HashModelMixin

from .column_types import ColumnPk, ColumnHash, ColumnText


class NameHashModelMixin(Md5HashModelMixin):
    __logger = logging.getLogger(__name__)

    __tablename__ = None

    __name_list__ = []

    __cashed__items__ = {}

    __cashed__dict__ = {}

    unique_hash_rule = dict(
        target_field_list=[
            'name'
        ],
        hash_field='name_md5'
    )

    id = ColumnPk()
    name = ColumnText()
    name_md5 = ColumnHash()

    @classmethod
    def by_name(cls, session, name):
        return session.query(cls).filter_by(name=name).first()

    @classmethod
    def by_name(cls, session, name):
        val = cls.__cashed__items__.get(name)
        if None == val:
            cls.__cashed__items__[name] = session.query(cls).filter_by(name=name).first()
            val = cls.__cashed__items__.get(name)
        return val

    @classmethod
    def cached_name(cls, session, name):
        val = cls.__cashed__dict__.get(name)
        if None == val:
            _clist = session.query(cls).all()
            for inst in _clist:
                cls.__cashed__dict__[inst.name] = inst
            val = cls.__cashed__dict__.get(name)
        return val

    @classmethod
    def by_name_list(cls, session, alist):
        return session.query(cls).filter(cls.name.in_(unique(alist))).all()

    @classmethod
    def prev_insert(cls, session):
        cname = cls.__name__
        cls.__logger.info('starts prev insert for %s...' % (cname))
        for name in cls.__name_list__:
            cls.as_unique(session, name=name)
            cls.__logger.debug('creates %s: %s' % (cname, name))
        session.commit()
        cls.__logger.info('ends prev insert for %s.' % (cname))
