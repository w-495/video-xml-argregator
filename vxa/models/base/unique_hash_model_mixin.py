#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from ..utils import SmartDict

from .base_model import BaseModel

def _unique(session, cls, args, kwargs):
    return _unique_fast(session, cls, args, kwargs)

def _unique_fast(session, cls, args, kwargs):
    cache = getattr(session, '_unique_cache', None)
    if cache is None:
        session._unique_cache = cache = {}
    hash_value = cls.get_hash_by_args(*args, **kwargs)
    key = (cls, cls.get_hash_by_args(*args, **kwargs))
    if key in cache:
        return cache[key]
    else:
        with session.no_autoflush:
            inserter = cls.__table__.insert().prefix_with("IGNORE")
            obj = cls(*args, **kwargs)
            session.execute(
                inserter,
                [vars(obj)]
            )
        cache[key] = obj
        return obj


class UniqueHashModelMixin(object):

    __table__ = None

    global_session = None

    unique_hash_rule = SmartDict(
        target_field_list =[],
        hash_field='name_hash'
    )

    default_string_for_empty_field = 'NULL'

    def __init__(self, *args, **kwargs):
        assert(isinstance(self, BaseModel))
        hash_field_value = self.get_hash_by_string(
            self.get_hash_source_string(*args, **kwargs)
        )
        super(UniqueHashModelMixin, self).__init__(*args, **kwargs)
        setattr(self, self.unique_hash_rule['hash_field'], hash_field_value)


    @classmethod
    def bulk_insert(cls, session, obj_list, *args, **kwargs):
        inserter = cls.__table__.insert().prefix_with("IGNORE")
        session.execute(
            inserter,
            [vars(obj) for obj in obj_list]
        )
        return obj_list


    @classmethod
    def get_hash_source_string(cls, *args, **kwargs):
        source_string = ''
        for target_field in cls.unique_hash_rule['target_field_list']:
            target_field_value = kwargs.get(target_field)
            if None == target_field_value:
                target_field_value = cls.default_string_for_empty_field
            source_string += u"%s"%(target_field_value)
        return source_string


    @classmethod
    def get_hash_by_args(cls, *args, **kwargs):
        return cls.get_hash_by_string(
            cls.get_hash_source_string(*args, **kwargs)
        )

    @classmethod
    def unique_filter(cls, query, *args, **kwargs):
        hash_value = cls.get_hash_by_args(*args, **kwargs)
        return query.filter(
            getattr(cls, cls.unique_hash_rule['hash_field']) == hash_value
        )

    @classmethod
    def get_hash_by_string(cls, string):
        return string

    @classmethod
    def as_unique(cls, session = None, *args, **kwargs):
        if not session:
            session = cls.get_session()
        return cls._as_unique(session, *args, **kwargs)

    @classmethod
    def _as_unique(cls, session, *args, **kwargs):
        return _unique(
            session,
            cls,
            args,
            kwargs
        )

    @classmethod
    def get_session(cls):
        return cls.global_session


    @classmethod
    def set_session(cls, session):
        cls.global_session = session
