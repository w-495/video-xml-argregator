#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from sqlalchemy import Column, ForeignKey

from .data_types import StringHash, StringUrl, StringLong, IntegerLong, Timestamp, DateTime


class ColumnPk(Column):
    def __init__(self, *args, **kwargs):
        super(ColumnPk, self).__init__(
            IntegerLong(),
            primary_key = True,
            nullable = False,
        )


class ColumnFk(Column):
    def __init__(self, fkname, *args, **kwargs):
        super(ColumnFk, self).__init__(
            IntegerLong(),
            ForeignKey(fkname),
            nullable = True,
            *args, **kwargs
        )

class ColumnFkHash(Column):
    def __init__(self, fkname, *args, **kwargs):
        super(ColumnFkHash, self).__init__(
            StringHash(),
            ForeignKey(fkname),
            nullable = True,
            *args, **kwargs
        )


class ColumnId(Column):
    def __init__(self, *args, **kwargs):
        super(ColumnId, self).__init__(
            IntegerLong(),
            nullable = True,
        )


class ColumnIntegerLong(Column):
    def __init__(self, *args, **kwargs):
        super(ColumnIntegerLong, self).__init__(
            IntegerLong(),
            nullable = True,
        )


class ColumnHash(Column):
    def __init__(self, *args, **kwargs):
        super(ColumnHash, self).__init__(
            StringHash(),
            nullable = True,
            unique = True,
        )


class ColumnText(Column):
    def __init__(self, *args, **kwargs):
        super(ColumnText, self).__init__(
            StringLong(),
            nullable = True,
        )

class ColumnUrl(Column):
    def __init__(self, *args, **kwargs):
        super(ColumnUrl, self).__init__(
            StringUrl(),
            nullable = True,
        )


class ColumnTimestamp(Column):
    def __init__(self, *args, **kwargs):
        super(ColumnTimestamp, self).__init__(
            Timestamp(),
            nullable = True,
        )


class ColumnDateTime(Column):
    def __init__(self, *args, **kwargs):
        super(ColumnDateTime, self).__init__(
            DateTime(),
            nullable = True,
        )
