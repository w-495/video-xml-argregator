#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import logging

from .base import NameHashModelMixin, BaseModel

class TagType(NameHashModelMixin, BaseModel):
    """
        drop table if exists tag_type;
        create table tag_type (
            id              bigint(20) unsigned     not null auto_increment,
            name            varchar(255)            default null,
            name_md5        binary(32)              default null,
            create_time     timestamp               default current_timestamp,
            primary key (id),
            unique name_md5 (name_md5)
        ) engine=myisam default charset=utf8;
    """

    __logger = logging.getLogger(__name__)

    __tablename__ = 'tag_type'

    __name_list__ = [
        'vendor_type',
        'status_type',
        'video_type',
        'adult_type',
        'allow_embed_type',
        'is_official_type',
        'license_type',
        'subscription_type',
        'platform_type',
        'person',
        'script',
        'actor',
        'director',
        'tag',
        'category',
        'genre',
        'language',
        'country',
        'allow_country',
        'disallow_country',
        'allow_platform',
        'disallow_platform',
        'login',
        'subtitle',
        'price',
        'dubbing',
        'resolution',
        'available_format',
        'production_company',
        'uploader',
        'year',
        'date',
        'album',
    ]
