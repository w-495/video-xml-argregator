# -*- coding: utf8 -*-

from __future__ import absolute_import, division, print_function

import six


def car(lst):
    return (lst or [None])[0]


def unique(a):
    seen = set()
    return [seen.add(x) or x for x in a if x not in seen]


class SmartString(six.text_type):
    def copy_vars(self, smart, *args, **kwargs):
        if None != smart:
            for name, value in six.iteritems(vars(smart)):
                setattr(self, name, value)
        return self


class SmartDict(dict):
    def __init__(self, *args, **kwargs):
        self.__dict__ = kwargs
        super(SmartDict, self).__init__(*args, **kwargs)

sstr = SmartString
