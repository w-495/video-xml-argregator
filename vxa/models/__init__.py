#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from .video_to_video import VideoToVideo

from .video_raw_to_tag import VideoRawToTag

from .video_raw import VideoRaw

from .tag_type import TagType

from .tag import Tag

from .orm import Orm

import logging

from vxa.settings.logging import start_logging

def init_db():
    start_logging()
    logging.info("starts init SQL database ...")
    TagType.prev_insert(Orm.con.session)
    # Tag.prev_insert(Orm.con.session)
    logging.info("end init SQL database.")

if __name__ == '__main__':
    init_db()
