#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import logging

from sqlalchemy.orm import relationship

from .base import MultiHashModelMixin, BaseModel, ColumnFk, ColumnPk, ColumnText, ColumnUrl


from .tag_type import TagType


class Tag(MultiHashModelMixin, BaseModel):
    """
        drop table if exists  tag;
        create table tag (
            id              bigint(20) unsigned     not null auto_increment,
            norm_tag_id     bigint(20) unsigned     default null,
            tag_type_id     bigint(20) unsigned     default null,
            name            text,
            description     text,
            vendor_url      varchar(255)            default null,
            hash_md5        binary(32)              default null,
            create_time     timestamp               default current_timestamp,
            primary key (id),
            unique hash_md5 (hash_md5)
        ) engine=myisam default charset=utf8;
    """

    __logger = logging.getLogger(__name__)

    __tablename__ = 'tag'

    id = ColumnPk()
    name = ColumnText()
    tag_type_id = ColumnFk('tag_type.id')
    vendor_url = ColumnUrl()
    tag_type = relationship('TagType', backref="tag_list")

    norm_tag_id = ColumnFk('tag.id')
    norm_tag = relationship(
        'Tag',
        backref="tag_list",
        remote_side=[id],
    )

    unique_hash_rule = dict(
        target_field_list=[
            'name',
            'tag_type_id',
        ],
        hash_field='hash_md5'
    )

    __name_list__ = [
        ('tvzavr.ru', 'vendor_type'),
        ('ivi.ru', 'vendor_type'),
        ('now.ru', 'vendor_type'),
        ('megogo.net', 'vendor_type'),
        ('tvigle.ru', 'vendor_type'),
        ('published', 'status_type'),
        ('private', 'status_type'),
        ('blocked', 'status_type'),
        ('processing', 'status_type'),
        ('deleted', 'status_type'),
        ('movie', 'video_type'),
        ('tvseries', 'video_type'),
        ('music', 'video_type'),
        ('tvshow', 'video_type'),
        ('broadcast', 'video_type'),
        ('adult', 'adult_type'),
        ('family_friendly', 'adult_type'),
        ('allow_embed', 'allow_embed_type'),
        ('disallow_embed', 'allow_embed_type'),
        ('official', 'is_official_type'),
        ('unofficial', 'is_official_type'),
        ('cc', 'license_type'),
        ('commercial', 'license_type'),
        ('subscription_required', 'subscription_type'),
        ('subscription_not_required', 'subscription_type'),
        ('Windows', 'platform_type'),
        ('Linux', 'platform_type'),
        ('OSX', 'platform_type'),
        ('iOS', 'platform_type'),
        ('Android', 'platform_type'),
        ('WindowsPhone', 'platform_type'),
        ('WindowsMobile', 'platform_type'),
        ('Symbian', 'platform_type'),
        ('bada', 'platform_type'),
        ('BlackBerry', 'platform_type'),
        ('MeeGo', 'platform_type'),
        ('Mobile', 'platform_type'),
        ('Desktop', 'platform_type'),
    ]

    @classmethod
    def prev_insert(cls, session):
        cls.__logger.info('starts prev insert ...')
        res = cls.prev_insert_mysql(session)
        cls.__logger.info('ends prev insert.')
        return res

    @classmethod
    def prev_insert_mysql(cls, session):
        cls.__logger.info('starts optimised (bulk) prev insert ...')

        if cls.__logger.isEnabledFor(logging.DEBUG):
            for tag, tag_type in cls.__name_list__:
                cls.__logger.debug('creates %s as %s'%(tag, tag_type))

        tag_type_list = TagType.by_name_list(
            session,
            [tag_type for tag, tag_type in cls.__name_list__]
        )
        tag_type_id_dict = {
            tag_type.name: tag_type.id
            for tag_type in tag_type_list
        }
        inserter = cls.__table__.insert().prefix_with("IGNORE")
        session.execute(
            inserter,
            [
                {
                    'name': tag,
                    'tag_type_id': tag_type_id_dict.get(tag_type),
                    'hash_md5': cls.get_hash_by_args(
                        name=tag,
                        tag_type_id=tag_type_id_dict.get(tag_type)
                    )
                }
                for tag, tag_type  in cls.__name_list__
            ]
        )
        cls.__logger.info('ends optimised (bulk) prev insert.')


    @classmethod
    def prev_insert_slow(cls, session):
        for name, type_name in cls.__name_list__:
            tag = cls.as_unique(
                session,
                name=name,
                tag_type=TagType.by_name(session, type_name)
            )
            session.add(tag)
        session.commit()
