#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import absolute_import, division, print_function

"""
GRANT
    SELECT,
    INSERT,
    UPDATE,
    DELETE,
    CREATE,CREATE TEMPORARY TABLES,
    DROP,
    INDEX,
    ALTER
ON
    video_xml_argregator.*
TO
    vxa@localhost
IDENTIFIED BY
    'video_xml_argregator';
"""

connection_params = dict(
    host='127.0.0.1',
    user='vxa',
    passwd='video_xml_argregator',
    db='video_xml_argregator',
)
