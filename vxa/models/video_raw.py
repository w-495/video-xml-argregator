#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from sqlalchemy.ext.associationproxy import association_proxy

from .base import MultiHashModelMixin, BaseModel, \
    ColumnPk, ColumnId, ColumnIntegerLong, \
    ColumnUrl, ColumnText, \
    ColumnDateTime, relationship


from .video_raw_to_tag import VideoRawToTag



class VideoRaw(MultiHashModelMixin, BaseModel):
    """
        drop table if exists video_raw;
        create table video_raw (
            id                          bigint(20) unsigned     not null auto_increment,
            hash_md5                    binary(32)              default null,
            content_id                  bigint(20) unsigned     default null,
            duration                    bigint(20) unsigned     default null,
            title                       text,
            parent_title                text,
            original_title              text,
            season                      bigint(20) unsigned     default null,
            episode                     bigint(20) unsigned     default null,
            part                        bigint(20) unsigned     default null,
            description                 text,
            create_date                 datetime null          default null,
            modify_date                 datetime null          default null,
            expiration_date             datetime null          default null,
            release_date                datetime null          default null,
            start_date                  datetime null          default null,
            end_date                    datetime null          default null,
            upload_date                 datetime null          default null,
            views_last_day              bigint(20) unsigned     default null,
            views_last_month            bigint(20) unsigned     default null,
            views_last_week             bigint(20) unsigned     default null,
            views_total                 bigint(20) unsigned     default null,
            comments                    bigint(20) unsigned     default null,
            likes                       bigint(20) unsigned     default null,
            dislikes                    bigint(20) unsigned     default null,
            rating                      bigint(20) unsigned     default null,
            rating_count                bigint(20) unsigned     default null,
            page_url                    varchar(255)            default null,
            player_url                  varchar(255)            default null,
            content_url                 varchar(255)            default null,
            thumbnail_url               varchar(255)            default null,
            poster_url                  varchar(255)            default null,
            feed_url                    varchar(255)            default null,
            gallery_url                 varchar(255)            default null,
            source_url                  varchar(255)            default null,
            vendor_url                  varchar(255)            default null,
            embed_html                  text,
            create_time                 timestamp               default current_timestamp,
            primary key (id),
            unique hash_md5 (hash_md5)
        ) engine=myisam default charset=utf8;
    """
    __tablename__ = 'video_raw'

    unique_hash_rule = dict(
        target_field_list=[
            'vendor_url',
            'source_url',
            'content_id',
            'page_url',
            'player_url',
            'content_url',
            'thumbnail_url',
            'poster_url',
            'feed_url',
            'gallery_url',
        ],
        hash_field='hash_md5'
    )

    id = ColumnPk('id')
    content_id = ColumnId()
    duration = ColumnIntegerLong()

    title = ColumnText()
    parent_title = ColumnText()
    original_title = ColumnText()
    description = ColumnText()

    season = ColumnIntegerLong()
    episode = ColumnIntegerLong()
    part = ColumnIntegerLong()

    create_date = ColumnDateTime()
    modify_date = ColumnDateTime()
    expiration_date = ColumnDateTime()
    release_date = ColumnDateTime()
    start_date = ColumnDateTime()
    end_date = ColumnDateTime()
    upload_date = ColumnDateTime()

    views_last_day = ColumnIntegerLong()
    views_last_month = ColumnIntegerLong()
    views_last_week = ColumnIntegerLong()
    views_total = ColumnIntegerLong()
    comments = ColumnIntegerLong()
    likes = ColumnIntegerLong()
    dislikes = ColumnIntegerLong()
    rating = ColumnIntegerLong()
    rating_count = ColumnIntegerLong()

    page_url = ColumnUrl()
    player_url = ColumnUrl()
    content_url = ColumnUrl()
    thumbnail_url = ColumnUrl()
    poster_url = ColumnUrl()
    feed_url = ColumnUrl()
    gallery_url = ColumnUrl()
    source_url = ColumnUrl()
    vendor_url = ColumnUrl()

    embed_html = ColumnText()

    tag_list = relationship(
        "Tag",
        secondary=VideoRawToTag.__table__,
        backref="video_list",
    )

    copy_list = association_proxy("video_copies", 'copy')
