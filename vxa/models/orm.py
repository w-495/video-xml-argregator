#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import scoped_session


from .base import BaseModel

from .config import connection_params


class ClassProperty(property):
    def __get__(self, obj, type=None):
        return self.fget.__get__(None, type)()

class Orm(object):

    __single_orm = None

    def __init__(self):
        self.engine = create_engine(
            'mysql://{user}:{passwd}@{host}/{db}?charset=utf8&use_unicode=0'.format(**connection_params),
            pool_size=50,
            max_overflow=0,
            echo=False
        )
        BaseModel.metadata.create_all(self.engine)
        self.session = self.get_session()


    def get_session(self):
        session_factory = sessionmaker(bind=self.engine)
        session = scoped_session(session_factory)
        return session()

    def get_scoped_session(self):
        session_factory = sessionmaker(bind=self.engine)
        session = scoped_session(session_factory)
        return session

    @ClassProperty
    @classmethod
    def con(cls):
        if cls.__single_orm is None:
            cls.__single_orm = cls()
        return cls.__single_orm
