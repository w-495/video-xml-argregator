#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

from .base import UniqueHashModelMixin, BaseModel, ColumnPk, ColumnFkHash


class VideoRawToTag(UniqueHashModelMixin, BaseModel):
    """
        drop table if exists video_raw_to_tag;
        create table video_raw_to_tag (
            id                          bigint(20) unsigned     not null auto_increment,
            video_raw_hash_md5          binary(32)              default null,
            tag_hash_md5                binary(32)              default null,
            create_time                 timestamp               default current_timestamp,
            primary key (id),
            unique video_raw_tag (video_raw_hash_md5, tag_hash_md5)
        ) engine=myisam default charset=utf8;
    """

    __tablename__ = 'video_raw_to_tag'

    id = ColumnPk()
    video_raw_hash_md5 = ColumnFkHash('video_raw.hash_md5')
    tag_hash_md5 = ColumnFkHash('tag.hash_md5')

    # video_raw = relationship('VideoRaw', backref="tag_list")
    # tag = relationship('Tag', backref="video_list")
