#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function


from .base import BaseModel, ColumnPk, ColumnFk, ColumnIntegerLong, relationship, backref
from .video_raw import VideoRaw


class VideoToVideo(BaseModel):
    """
        drop table if exists video_to_video;
        create table video_to_video (
            id                          bigint(20) unsigned     not null auto_increment,
            video_id                    bigint(20) unsigned     default null,
            copy_video_id               bigint(20) unsigned     default null,
            version                     bigint(20) unsigned     not null default '0',
            create_time                 timestamp               default current_timestamp,
            primary key (id),
            unique video_id_copy_video_id (video_id, copy_video_id)
        ) engine=myisam default charset=utf8;
    """

    __tablename__ = 'video_to_video'

    id = ColumnPk()
    video_id = ColumnFk('video_raw.id')
    copy_video_id = ColumnFk('video_raw.id')
    version = ColumnIntegerLong()

    video = relationship(
        VideoRaw,
        backref=backref(
            "video_copies",
            cascade="all, delete-orphan"
        ),
        foreign_keys=[video_id]
    )

    copy = relationship(VideoRaw, foreign_keys=[copy_video_id])
