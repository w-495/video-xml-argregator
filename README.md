
	apt-get install python-scrapy
	sudo apt-get install python-dateutil


# How to start

## How to initialise database

    python -m vxa.models.__init__

## How to crawl video

    scrapy crawl <spider name>

Avaliable spiders:

* tvzavr.ru
* ivi.ru
* now.ru
* tvigle.ru
* megogo.net

Example:

    scrapy crawl tvzavr.ru
    
    
## How to merge serials

    python -m vxa.analyser.merger

## How to normalize person names

    python -m vxa.analyser.person_normalizer
   
Now.ru uses person names as «Surname Firstname» and others uses it like «Firstname Surname».
We exploit now.ru names as pivot to normalize other names.

+++


## How to normalize category names

    python -m vxa.analyser.category_normalizer

It Builds initial form of category tags and write it into data base.
For example:       
 
    «Cats» -> «cat»
            




